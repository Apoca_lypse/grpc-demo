package pri.wkz.remotes;

import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pri.wkz.GrpcClientApplication;
import pri.wkz.demo.grpc.endpoint.HelloServiceGrpc;
import pri.wkz.demo.grpc.model.ModelProto;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = GrpcClientApplication.class)
class HelloServiceTest {
    Logger log = org.slf4j.LoggerFactory.getLogger(HelloServiceTest.class);
    @Autowired
    private HelloService helloService;

    @Test
    void sayHello() {
        String response = helloService.sayHello("World", 1);
        log.info("response: {}", response);
    }

    @GrpcClient("global")
    private HelloServiceGrpc.HelloServiceStub helloServiceStub;

    @Test
    void serverTIme(){
        ModelProto.HelloRequest request = ModelProto.HelloRequest.newBuilder()
                .setMessage("World")
                .setCode(1)
                .build();
        helloServiceStub.serverTime(request, new StreamObserver<ModelProto.HelloResponse>() {
            @Override
            public void onNext(ModelProto.HelloResponse value) {
                log.info("response: {}", value.getMessage());
            }

            @Override
            public void onError(Throwable t) {
                log.error("error", t);
            }

            @Override
            public void onCompleted() {
                log.info("completed");
            }
        });
        while (true){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}