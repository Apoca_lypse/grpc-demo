package pri.wkz.remotes;

import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Component;
import pri.wkz.demo.grpc.endpoint.HelloServiceGrpc;
import pri.wkz.demo.grpc.model.ModelProto;

@Component
public class HelloService {
    @GrpcClient("global")
    private HelloServiceGrpc.HelloServiceBlockingStub helloServiceBlockingStub;

    public String sayHello(String message, int code) {
        ModelProto.HelloRequest request = ModelProto.HelloRequest.newBuilder()
                .setMessage(message)
                .setCode(code)
                .build();
        ModelProto.HelloResponse helloResponse = helloServiceBlockingStub.sayHello(request);
        return helloResponse.getMessage();
    }
}
