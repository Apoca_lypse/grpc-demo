package pri.wkz;


import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pri.wkz.demo.grpc.model.ModelProto;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class VersionTest {
    static Logger log = LoggerFactory.getLogger(VersionTest.class);

    @Test
    void testUpdateBefore() throws IOException {
        ModelProto.ForAdd forAdd = ModelProto.ForAdd.newBuilder()
                .setAdda(1)
                .setAddb(2)
                .build();
        ModelProto.ForRemove forRemove = ModelProto.ForRemove.newBuilder()
                .setRemovea(1)
                .setRemoveb(2)
                .setRemovec(3)
                .build();

        try (FileOutputStream forAddOut = new FileOutputStream("forAdd.bin");
             FileOutputStream forRemoveOut = new FileOutputStream("forRemove.bin");) {
            forAddOut.write(forAdd.toByteArray());
            forAddOut.flush();
            forRemoveOut.write(forRemove.toByteArray());
            forRemoveOut.flush();
        }
    }

    @Test
    void testBeforeRead() throws IOException {
        try (FileInputStream forAddIn = new FileInputStream("forAdd.bin");
             FileInputStream forRemoveIn = new FileInputStream("forRemove.bin");) {
            ModelProto.ForAdd forAdd = ModelProto.ForAdd.parseFrom(forAddIn);
            ModelProto.ForRemove forRemove = ModelProto.ForRemove.parseFrom(forRemoveIn);
            log.info("forAdd: {}", forAdd);
            log.info("forRemove: {}", forRemove);
        }
    }

    @Test
    void testAfterRead() throws IOException {
        try (FileInputStream forAddIn = new FileInputStream("forAdd.bin");
             FileInputStream forRemoveIn = new FileInputStream("forRemove.bin");) {
            ModelProto.ForAdd forAdd = ModelProto.ForAdd.parseFrom(forAddIn);
            ModelProto.ForRemove forRemove = ModelProto.ForRemove.parseFrom(forRemoveIn);
            log.info("forAdd: {}", forAdd);
            log.info("forRemove: {}", forRemove);
        }
    }
}
