package pri.wkz;

import com.google.protobuf.Any;
import com.google.protobuf.BoolValue;
import com.google.protobuf.Descriptors;
import com.google.protobuf.Int32Value;
import com.google.protobuf.Int64Value;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import pri.wkz.demo.grpc.model.ModelProto;

import java.util.Arrays;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;


class ManITest {

    private final static Logger log = org.slf4j.LoggerFactory.getLogger(ManITest.class);

    @Test
    void testModelProto() {
        ModelProto.HelloRequest request = ModelProto.HelloRequest.newBuilder()
                .setMessage("World")
                .setCode(1)
                .build();
        log.debug("Request: {}", request);
        ModelProto.EnumMessage enumMessage = ModelProto.EnumMessage.forNumber(1);
    }

    @Test
    void testOptionalModel() {
        ModelProto.OptionMessage build = ModelProto.OptionMessage.newBuilder()
                .build();
        Map<Descriptors.FieldDescriptor, Object> allFields = build.getAllFields();
        for (Map.Entry<Descriptors.FieldDescriptor, Object> entry : allFields.entrySet()) {
            log.debug("Field: {}, Value: {}", entry.getKey().getName(), entry.getValue());
        }
    }

    @Test
    void testCollectionModel() {
        ModelProto.CollectionMessage build = ModelProto.CollectionMessage.newBuilder()
                .addAllListField(Arrays.asList("1", "2", "3"))
                .putMapField(1, "1")
                .build();
        System.out.println(build);
    }

    @Test
    void nestestModel() {
        ModelProto.OutMessage build = ModelProto.OutMessage.newBuilder()
                .addInners(ModelProto.OutMessage.InnerMessage.newBuilder()
                        .setInner1("1")
                        .setInner2(2)
                        .build())
                .addInners(ModelProto.OutMessage.InnerMessage.newBuilder()
                        .setInner1("3")
                        .setInner2(4)
                        .build())
                .build();
        System.out.println(build);
    }

    @Test
    void oneOfModel() {
        ModelProto.ChartBarSearch build = ModelProto.ChartBarSearch.newBuilder()
                .setSymbol("USDJPY")
                .setTo(System.currentTimeMillis())
                .setFrom(Int64Value.of(System.currentTimeMillis() - 1000 * 60 * 60))
                .setIsAll(BoolValue.of(true))
                .setCount(Int32Value.of(50))
                .build();
        ModelProto.ChartBarSearch.LimitCase limitCase = build.getLimitCase();
        switch (limitCase){
            case FROM -> {
            }
            case COUNT -> {}
            case IS_ALL -> {}
            case LIMIT_NOT_SET -> {}
        }
        System.out.println(build);
    }

}