package wkz;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import pri.wkz.demo.grpc.model.ModelProto;

import static org.junit.jupiter.api.Assertions.*;


class ManITest {

    private final static Logger log = org.slf4j.LoggerFactory.getLogger(ManITest.class);

    @Test
    void testModelProto() {
        ModelProto.HelloRequest request = ModelProto.HelloRequest.newBuilder()
                .setMessage("World")
                .build();
        log.debug("Request: {}", request);
    }

}