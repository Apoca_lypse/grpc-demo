package pri.wkz.endpoint;

import com.google.protobuf.Empty;
import com.google.protobuf.StringValue;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import pri.wkz.demo.grpc.endpoint.SpecificConditionServiceGrpc;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@GrpcService
public class SpecificConditionEndpoint extends SpecificConditionServiceGrpc.SpecificConditionServiceImplBase {

    @Override
    public void frequentPush(Empty request, StreamObserver<StringValue> responseObserver) {
        new Thread(() -> {
            while (true) {
                LocalDateTime now = LocalDateTime.now();
                String format = now.format(DateTimeFormatter.ISO_DATE_TIME);
                StringValue stringValue = StringValue.newBuilder()
                        .setValue(format)
                        .build();
                responseObserver.onNext(stringValue);
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }).start();
    }
}
