package pri.wkz.endpoint;

import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import pri.wkz.demo.grpc.endpoint.HelloServiceGrpc;
import pri.wkz.demo.grpc.model.ModelProto;

import java.text.SimpleDateFormat;
import java.util.Date;

@GrpcService
public class HelloEndpoint extends HelloServiceGrpc.HelloServiceImplBase {

    @Override
    public void sayHello(ModelProto.HelloRequest request, StreamObserver<ModelProto.HelloResponse> responseObserver) {
        String message = request.getMessage();
        int code = request.getCode();
        ModelProto.HelloResponse response = ModelProto.HelloResponse.newBuilder()
                .setMessage("Hello " + message + ":" + code)
                .build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }


    @Override
    public void serverTime(ModelProto.HelloRequest request, StreamObserver<ModelProto.HelloResponse> responseObserver) {
        String message = request.getMessage();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (int i = 0; i < 10; i++) {
            Date date = new Date();
            ModelProto.HelloResponse response = ModelProto.HelloResponse.newBuilder()
                    .setMessage("Hello " + message + ":" + sdf.format(date))
                    .build();
            responseObserver.onNext(response);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        responseObserver.onCompleted();
    }
}
