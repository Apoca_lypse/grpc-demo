package pri.wkz.endpoint;

import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pri.wkz.demo.grpc.endpoint.ProtoStudyServiceGrpc;
import pri.wkz.demo.grpc.model.ModelProto;

@GrpcService
public class ProtoStudyEndpoint extends ProtoStudyServiceGrpc.ProtoStudyServiceImplBase {
    private final static Logger log = LoggerFactory.getLogger(ProtoStudyEndpoint.class);

    @Override
    public void optionalRequest(ModelProto.OptionMessage request, StreamObserver<ModelProto.OptionMessage> responseObserver) {
        //  string str_Field = 1;
        //  optional string str_optional_Field = 2;
        //  int32 int_Field = 3;
        //  optional int32 int_optional_Field = 4;
        //  int64 long_Field = 5;
        //  optional int64 long_optional_Field = 6;
        //  double double_Field = 7;
        //  optional double double_optional_Field = 8;
        //  bool bool_Field = 101;
        //  optional bool bool_optional_Field = 102;
        //  bytes bytes_Field = 103;
        //  optional bytes bytes_optional_Field = 104;
        //  HelloResponse obj_field = 9;
        //  optional HelloResponse obj_optional_field = 10;
        //  EnumMessage enum_field = 11;
        //  optional EnumMessage enum_optional_field = 12;
        //  google.protobuf.StringValue wrapper_str_field = 13;
        //  optional google.protobuf.StringValue wrapper_str_optional_field = 14;
        //  google.protobuf.Int32Value wrapper_int_field = 15;
        //  optional google.protobuf.Int32Value wrapper_int_optional_field = 16;
        //  google.protobuf.Int64Value wrapper_long_field = 17;
        //  optional google.protobuf.Int64Value wrapper_long_optional_field = 18;
        //  google.protobuf.DoubleValue wrapper_double_field = 19;
        //  optional google.protobuf.DoubleValue wrapper_double_optional_field = 20;
        //  google.protobuf.BoolValue wrapper_bool_field = 21;
        //  optional google.protobuf.BoolValue wrapper_bool_optional_field = 22;
        //  google.protobuf.BytesValue wrapper_bytes_field = 23;
        //  optional google.protobuf.BytesValue wrapper_bytes_optional_field = 24;
        log.info("str_field: true, {}", request.getStrField());
        log.info("str_optional_Field: {}, {}", request.hasStrOptionalField(), request.getStrOptionalField());
        log.info("int_Field: true, {}", request.getIntField());
        log.info("int_optional_Field: {}, {}", request.hasIntOptionalField(), request.getIntOptionalField());
        log.info("long_Field: true, {}", request.getLongField());
        log.info("long_optional_Field: {}, {}", request.hasLongOptionalField(), request.getLongOptionalField());
        log.info("double_Field: true, {}", request.getDoubleField());
        log.info("double_optional_Field: {}, {}", request.hasDoubleOptionalField(), request.getDoubleOptionalField());
        log.info("bool_Field: true, {}", request.getBoolField());
        log.info("bool_optional_Field: {}, {}", request.hasBoolOptionalField(), request.getBoolOptionalField());
        log.info("bytes_Field: true, {}", request.getBytesField());
        log.info("bytes_optional_Field: {}, {}", request.hasBytesOptionalField(), request.getBytesOptionalField());
        log.info("obj_field: {}, {}", request.hasObjField(), request.getObjField());
        log.info("obj_optional_field: {}, {}", request.hasObjOptionalField(), request.getObjOptionalField());
        log.info("enum_field: true, {}", request.getEnumField());
        log.info("enum_optional_field: {}, {}", request.hasEnumOptionalField(), request.getEnumOptionalField());
        log.info("wrapper_str_field: {}, {}", request.hasWrapperStrField(), request.getWrapperStrField());
        log.info("wrapper_str_optional_field: {}, {}", request.hasWrapperStrOptionalField(), request.getWrapperStrOptionalField());
        log.info("wrapper_int_field: {}, {}", request.hasWrapperIntField(), request.getWrapperIntField());
        log.info("wrapper_int_optional_field: {}, {}", request.hasWrapperIntOptionalField(), request.getWrapperIntOptionalField());
        log.info("wrapper_long_field: {}, {}", request.hasWrapperLongField(), request.getWrapperLongField());
        log.info("wrapper_long_optional_field: {}, {}", request.hasWrapperLongOptionalField(), request.getWrapperLongOptionalField());
        log.info("wrapper_double_field: {}, {}", request.hasWrapperDoubleField(), request.getWrapperDoubleField());
        log.info("wrapper_double_optional_field: {}, {}", request.hasWrapperDoubleOptionalField(), request.getWrapperDoubleOptionalField());
        log.info("wrapper_bool_field: {}, {}", request.hasWrapperBoolField(), request.getWrapperBoolField());
        log.info("wrapper_bool_optional_field: {}, {}", request.hasWrapperBoolOptionalField(), request.getWrapperBoolOptionalField());
        log.info("wrapper_bytes_field: {}, {}", request.hasWrapperBytesField(), request.getWrapperBytesField());
        log.info("wrapper_bytes_optional_field: {}, {}", request.hasWrapperBytesOptionalField(), request.getWrapperBytesOptionalField());

        ModelProto.OptionMessage build = ModelProto.OptionMessage.newBuilder()
                .setEnumField(ModelProto.EnumMessage.Option4)
                .setObjField(ModelProto.HelloResponse.newBuilder().setMessage("233").build())
                .build();
        responseObserver.onNext(build);
        responseObserver.onCompleted();
    }
}
