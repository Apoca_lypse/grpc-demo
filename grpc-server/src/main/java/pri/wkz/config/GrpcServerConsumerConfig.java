package pri.wkz.config;

import io.grpc.ServerBuilder;
import net.devh.boot.grpc.server.serverfactory.GrpcServerConfigurer;
import org.springframework.context.annotation.Configuration;
import pri.wkz.interceptor.LogInterceptor;

@Configuration
public class GrpcServerConsumerConfig implements GrpcServerConfigurer {

    @Override
    public void accept(ServerBuilder<?> serverBuilder) {
        serverBuilder.intercept(new LogInterceptor());
    }
}
