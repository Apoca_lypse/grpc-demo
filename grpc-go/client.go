package main

import (
    "context"
    "log"
    "time"
    "io"
    "google.golang.org/grpc"
    pb "grpc-go/proto_generated" // 替换为实际路径
)

const (
    address     = "localhost:9092"
    defaultName = "world"
)

func main() {
    conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
    if err != nil {
        log.Fatalf("did not connect: %v", err)
    }
    defer conn.Close()
    c := pb.NewHelloServiceClient(conn)

    // 简单的RPC
    r, err := c.SayHello(context.Background(), &pb.HelloRequest{Message: "Hello gRPC Server"})
    if err != nil {
        log.Fatalf("could not greet: %v", err)
    }
    log.Printf("Greeting: %s", r.GetMessage())

    // 服务器流式RPC
    stream, err := c.ServerTime(context.Background(), &pb.HelloRequest{Message: "Server Time Request"})
    if err != nil {
        log.Fatalf("could not get server time: %v", err)
    }
    for {
        r, err := stream.Recv()
        if err == io.EOF {
            // 服务器完成发送
            break
        }
        if err != nil {
            log.Fatalf("stream error: %v", err)
        }
        log.Printf("Server Time Response: %s", r.GetMessage())
        time.Sleep(5 * time.Second) // 每秒接收一次
    }
}