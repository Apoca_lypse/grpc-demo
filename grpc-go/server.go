package main

import (
    "context"
    "log"
    "net"
    "google.golang.org/grpc"
    pb "grpc-go/proto_generated" // 替换为实际路径
)

const (
    port = ":9094"
)

type server struct {
    pb.UnimplementedHelloServiceServer
}

func (s *server) SayHello(ctx context.Context, in *pb.HelloRequest) (*pb.HelloResponse, error) {
    log.Printf("Received: %v", in.GetMessage())
    return &pb.HelloResponse{Message: "Hello " + in.Message}, nil
}



func main() {
    lis, err := net.Listen("tcp", port)
    if err != nil {
        log.Fatalf("failed to listen: %v", err)
    }
    s := grpc.NewServer()
    pb.RegisterHelloServiceServer(s, &server{})
    log.Printf("server listening at %v", lis.Addr())
    if err := s.Serve(lis); err != nil {
        log.Fatalf("failed to serve: %v", err)
    }
}