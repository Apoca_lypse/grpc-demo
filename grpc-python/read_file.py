from proto_generated import model_pb2

# 创建消息的实例
for_add_msg = model_pb2.ForAdd()
for_remove_msg = model_pb2.ForRemove()

# 从文件中读取字节串
with open(r'D:\dev\codes_demo\java\grpc-demo2\grpc-common\forAdd.bin', 'rb') as file:
    for_add_bytes = file.read()

with open(r'D:\dev\codes_demo\java\grpc-demo2\grpc-common\forRemove.bin', 'rb') as file:
    for_remove_bytes = file.read()

# 使用字节串构建Protobuf消息
for_add_msg.ParseFromString(for_add_bytes)
for_remove_msg.ParseFromString(for_remove_bytes)

# 现在可以使用message对象了
print("for_add_msg:", for_add_msg, for_add_bytes)
print("for_remove_msg:", for_remove_msg, for_remove_bytes)
