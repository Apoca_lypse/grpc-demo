import grpc
import asyncio
from proto_generated import request_pb2_grpc as pb2_grpc
from proto_generated import model_pb2
import datetime as dt


class HelloService(pb2_grpc.HelloServiceServicer):

    async def SayHello(self, request: model_pb2.HelloRequest, context):
        print(request)
        return model_pb2.HelloResponse(message=f"Hello, {request.message}: {request.code}")

    async def serverTime(self, request: model_pb2.HelloRequest, context):
        print(request)
        for i in range(10):
            await asyncio.sleep(1)
            dt_str = dt.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            yield  model_pb2.HelloResponse(message=f"{request.message}:{dt_str}")


async def async_serve():
    server = grpc.aio.server()
    port = 9093
    listen_addr = f'[::]:{port}'
    server.add_insecure_port(listen_addr)
    pb2_grpc.add_HelloServiceServicer_to_server(HelloService(), server)
    await server.start()
    await server.wait_for_termination()


if __name__ == '__main__':
    asyncio.run(async_serve())
