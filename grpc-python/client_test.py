from proto_generated import model_pb2
from proto_generated import request_pb2_grpc as pb2_grpc
import grpc
import unittest as ut
from google.protobuf import empty_pb2 as google_dot_protobuf_dot_empty__pb2

java_port = 11090
port = java_port
test_url = f"localhost:{port}"


class GrpcClientTest(ut.TestCase):

    def test_say_hello(self):
        with grpc.insecure_channel(test_url) as channel:
            stub = pb2_grpc.HelloServiceStub(channel)
            response = stub.SayHello(model_pb2.HelloRequest(message="python", code=1))
            print(response)

    def test_time_server(self):
        with grpc.insecure_channel(test_url) as channel:
            stub = pb2_grpc.HelloServiceStub(channel)
            response = stub.serverTime(model_pb2.HelloRequest(message="python", code=1))
            for item in response:
                print(item)

    def test_fluent_push(self):
        with grpc.insecure_channel(test_url) as channel:
            stub = pb2_grpc.SpecificConditionServiceStub(channel)

            resp_iter = stub.FrequentPush(google_dot_protobuf_dot_empty__pb2.Empty())

            for item in resp_iter:
                print(item)


class OptionalClientTest(ut.TestCase):
    def test_optional_(self):
        print("conncting...")
        with grpc.insecure_channel(test_url) as channel:
            stub = pb2_grpc.ProtoStudyServiceStub(channel)
            response = stub.OptionalRequest(model_pb2.OptionMessage(int_field=1, int_optional_field=2))
            print("resp:", response)


if __name__ == '__main__':
    ut.main()
