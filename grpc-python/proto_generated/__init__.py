import os
import sys

# 解决proto生成的代码pb2_grpc导入pb2时找不到的问题
project_root = os.path.abspath(os.path.dirname(__file__))
sys.path.append(project_root)